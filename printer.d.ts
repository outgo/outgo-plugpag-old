export declare class PlugPagPrinter {
    /**
     * Imprime um imagem, fornecida em base64, pelo plugin da PlugPag.
     * @param image A imagem, em base64.
     * @returns Uma promessa que resolve quando a imagem for enviada para impressão.
     */
    printImage(image: string): Promise<void>;
    /**
     * Imprime um texto pelo plugin da PlugPag.
     * @param text O texto a ser impresso.
     * @returns Uma promessa que resolve quando o texto for enviado para impressão.
     */
    printText(...text: string[]): Promise<void>;
}
