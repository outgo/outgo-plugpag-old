var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from "@angular/core";
import * as QRCodeGenerator from "qrcode";
var PlugPagPrinter = /** @class */ (function () {
    function PlugPagPrinter() {
    }
    /**
     * Imprime um imagem, fornecida em base64, pelo plugin da PlugPag.
     * @param image A imagem, em base64.
     * @returns Uma promessa que resolve quando a imagem for enviada para impressão.
     */
    PlugPagPrinter.prototype.printImage = function (image) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!window.plugPag) {
                    console.warn("Plugin PlugPag não esta instalado!");
                    return [2 /*return*/];
                }
                else {
                    return [2 /*return*/, window.plugPag.print(image)];
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Imprime um texto pelo plugin da PlugPag.
     * @param text O texto a ser impresso.
     * @returns Uma promessa que resolve quando o texto for enviado para impressão.
     */
    PlugPagPrinter.prototype.printText = function () {
        var text = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            text[_i] = arguments[_i];
        }
        return __awaiter(this, void 0, void 0, function () {
            var graphics_1, lineHeight, charactersPerLine, qrCodeWidth_1, bonusHeight, currentLine_1, i, line, words, phrase1, phrase2, word, spacesNeeded, _loop_1, i, image;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!window.plugPag) return [3 /*break*/, 1];
                        console.warn("Plugin PlugPag não esta instalado!");
                        return [2 /*return*/];
                    case 1:
                        graphics_1 = document.createElement("canvas").getContext("2d");
                        lineHeight = 40;
                        charactersPerLine = 18;
                        qrCodeWidth_1 = 280;
                        bonusHeight = 0;
                        currentLine_1 = lineHeight;
                        for (i = 0; i < text.length; i++) {
                            line = text[i];
                            if (!line)
                                continue;
                            //Identificar tags
                            if (line.startsWith("<qrcode>")) {
                                bonusHeight += qrCodeWidth_1;
                                continue;
                            }
                            while (line.length > charactersPerLine) {
                                words = line.split(" ");
                                phrase1 = void 0, phrase2 = void 0;
                                if (words.length == 1) {
                                    word = words[0];
                                    phrase1 = word.substring(0, word.length / 2);
                                    phrase2 = word.substring(word.length / 2, word.length);
                                }
                                else {
                                    phrase1 = words.shift();
                                    phrase2 = words.pop();
                                    while (words.length > 0) {
                                        if (phrase1.length > phrase2.length) {
                                            phrase2 = words.pop() + " " + phrase2;
                                        }
                                        else {
                                            phrase1 += " " + words.shift();
                                        }
                                    }
                                }
                                line = phrase1;
                                text.splice(i + 1, 0, phrase2);
                            }
                            spacesNeeded = charactersPerLine - line.length;
                            if (spacesNeeded > 0) {
                                line = " ".repeat(spacesNeeded / 2) + line;
                            }
                            text[i] = line;
                        }
                        graphics_1.canvas.width = 450;
                        graphics_1.canvas.height = (lineHeight * text.length) + bonusHeight;
                        graphics_1.fillStyle = "white";
                        graphics_1.fillRect(0, 0, graphics_1.canvas.width, graphics_1.canvas.height);
                        graphics_1.fillStyle = "black";
                        graphics_1.font = "40px Courier";
                        _loop_1 = function (i) {
                            var line, qrcode, qrcodeImage_1, qrcodePromise, spacesNeeded;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        line = text[i];
                                        if (!line) return [3 /*break*/, 4];
                                        if (!line.startsWith("<qrcode>")) return [3 /*break*/, 3];
                                        qrcode = line.replace("<qrcode>", "").replace("</qrcode>", "");
                                        return [4 /*yield*/, QRCodeGenerator.toDataURL(qrcode, {
                                                width: qrCodeWidth_1
                                            })];
                                    case 1:
                                        qrcodeImage_1 = _a.sent();
                                        qrcodePromise = new Promise(function (resolve, reject) {
                                            var image = new Image();
                                            image.onload = function () {
                                                graphics_1.drawImage(image, (graphics_1.canvas.width - qrCodeWidth_1) / 2, currentLine_1 - 10);
                                                resolve();
                                            };
                                            image.src = qrcodeImage_1;
                                        });
                                        return [4 /*yield*/, qrcodePromise];
                                    case 2:
                                        _a.sent();
                                        currentLine_1 += qrCodeWidth_1;
                                        spacesNeeded = charactersPerLine - qrcode.length;
                                        if (spacesNeeded > 0) {
                                            qrcode = " ".repeat(spacesNeeded / 2) + qrcode;
                                        }
                                        graphics_1.fillText(qrcode, 10, currentLine_1);
                                        return [3 /*break*/, 4];
                                    case 3:
                                        graphics_1.fillText(line, 10, currentLine_1);
                                        _a.label = 4;
                                    case 4:
                                        currentLine_1 += lineHeight;
                                        return [2 /*return*/];
                                }
                            });
                        };
                        i = 0;
                        _a.label = 2;
                    case 2:
                        if (!(i < text.length)) return [3 /*break*/, 5];
                        return [5 /*yield**/, _loop_1(i)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        i++;
                        return [3 /*break*/, 2];
                    case 5:
                        ;
                        image = graphics_1.canvas.toDataURL().split("base64,")[1];
                        return [2 /*return*/, window.plugPag.print(image)];
                }
            });
        });
    };
    PlugPagPrinter = __decorate([
        Injectable({ providedIn: "root" })
    ], PlugPagPrinter);
    return PlugPagPrinter;
}());
export { PlugPagPrinter };
